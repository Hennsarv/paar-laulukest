﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Muusika
{
    class Program
    {
        static void Laula(string s)
        {
            string noodid = "C.D.EF.G.A.H";

            Dictionary<char, int> sagedused =
            new Dictionary<char, int>
            {
                { 'C', 440},
                { 'D', 493},
                { 'E', 554},
                { 'F', 587},
                { 'G', 659},
                { 'A', 739},
                { 'H', 830},
                { 'P', 37 }
            };


            foreach (var x in s)
            {
                //              if (x == 'P') Task.Delay(500).Wait();
//                if (x == 'P') Thread.Sleep(500);
                //else
                //{
                //    int i = noodid.IndexOf(x);
                //    int j = (int)(440 * Math.Pow(2, i / 12.0));
                //    Console.Beep(j, 300);
                //}
//                else 
                Console.Beep(sagedused[x], 500);
            }
        }

        static void Main(string[] args)
        {

            double pooltoon = Math.Pow(2, 1 / 12);
            /* c = 0, d = 2, e = 4, f = 5, g = 7, a = 9, h = 11, c = 12*/
            // p on paus
            string jubalinnukesed_salm1 = "GEGEGPGPFEFGEPPPGEGEGPGPFEFGEPPP";
            string jubalinnukesed_salm2 = "EEEEFEDCDDDFEDCPPPEEEEFEDCDDDFEDCPPP";
            string rongisõit = "CDEFGGGPCDEFGGGPFFFFEEEPDDDDCCCPFFFPEEEPDDDDCCCP";
            string sepapoisid = "CPDPEPCPCPDPEPCPEPFPGPPPEPFPGPPP" +
                                "GAGFEPCPGAGFEPCPCPDECPPPCPDECPPP";

            Laula(rongisõit);

            for (int i = 1000; i < 25; i++)
            {
                int j = (int)(440 * Math.Pow(2, i / 12.0));
                Console.Beep(j, 100);
            }





        }
    }
}
